2a. 
#!/bin/bash
# Script name read.sh
FILE=$1
input=$FILE
while IFS= read -r line
do
  echo "$line"
done < "$input"

# To run type 
$ ./read.sh ./ttt

2b. ulimit -n 1024
2c.
DOCKER_FLAGS="-ti ubuntu:latest cat /etc/resolv.conv"
docker run "${DOCKER_FLAGS}"