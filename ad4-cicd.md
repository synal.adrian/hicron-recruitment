Nie mam zbyt duzego doswiadczenia z ww. narzedziami (Maven, Artifactory) ale spedzilem chwile z dokumentacja i probowal bym rozwiazac ten problem tak.

1. W ustawieniach mojego pipeline dodał bym dwie zmienne username i password. 
2. W katalogu głównym projektu dodał bym katalog ./m2 a w nim plik settings xml.
```
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd" xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <servers>
    <server>
      <id>central</id>
      <username>${env.ARTIFACTORY_USER}</username>
      <password>${env.ENCRYPTED_PASS}</password>
    </server>
  </servers>
</settings>
```
3. Plik .gitlab-ci.yml wtedy wyglądał by mniej więcej tak: 
```
image: maven:latest

variables:
  MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  paths:
    - .m2/repository/
    - target/

build:
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS compile
```

