
Z uwagi na dość krótki czas jaki mam nie mogę tego nazwać projektem architektury, bardziej powiedział bym że jest to opis projektu, bądź kierunek w którym szedł bym projektując rozwiązanie. 

Ponieważ mam większe doświadczenie z AWS niż Azure, posłużę się tym pierwszym. 
Przyznam szczerze, że nigdy nie projektowałem ani nie wdrażałem takiego rozwiązania. Jeśli miał bym realnie to zrobić, rozwiazał bym to tak jak niżej opisałem. 

Frontend - pliki statycznej strony umieścił bym na buckecie s3, pliki udostępnione publicznie. Bucket z włączoną opcją "Static website hosting". Publiczną domenę na której ma działać witryna dodał bym do Route53 - to ułatwi późniejszą konfigurację. Na potrzeby naszej domeny musimy wygenerować certyfikat ssl w AWS Certificate Manager. Na froncie ustawił bym CDN-a (Cloud front), jako origin domain należało by ustawić nasz s3 bucket w którym znajdują sie pliki. Jako alternate domain naszą domenę z Route53, dodać wygenerowany certyfikat SSL oraz wskazać Default Root Object jako index.html. W ustawieniach naszej domeny publicznej, w hosted zonie dodał bym rekord A wskazując adres naszej dystrybucji z CloudFront. 

Backend - kontenery docker umiesclił bym w klastrze ecs co da nam samo skalowalność w zależności od ruchu na stronie. (ECS Cluster, Service i Task Definition)

Bazy danych - Bazy danych SQL umieścił bym w usłudze RDS. Na potrzeby zarządzania sesjami utworzył bym cluster Redis w aws. 

Upload plików - tę funkcjonalność zimplementował bym poprzez dodanie dodatkowego bucketu s3, innego niż wcześniej stworzony, który utrzymywał by jedynie user content. 

Żdeby wszystko ze sobą zaczęło działać należy stworzyć odpowiednie security groups np. web-sg, app-sq, db-sq. Dodać odpowiednie reguły dla portów po których backend będzie się komunikował z bazami danych. Jeśli chodzi o dostęp do zasobów w s3 użył bym IAM roli z custom managed policies. 

Tak po krótce mógł bym opisać wizję tego, jak miało by to wyglądać. Niestety z projektowaniem aplikacji webowych nie mam dużego doświadczenia, przy realnym scenariuszu na pewno musiał bym spędzić kilka nocy wertując best practices, szukając przykładów architektury i implementacji. 