Utawił bym tunel z lokalnej maszyny do hosta jumpbox.local przykładowo na porcie 2999, następnie tunel z jumpbox.local do app01.local forwardując port 8080. 
W efekcie aplikacja webowa "X" powinna być dostępna na mojej lokalnej maszynie na porcie 2999. 
Żeby uzyskać dostęp do aplikacji przez przeglądarkę dodał bym wpis w hosts, na przykład 172.0.0.1 x.domain.com i próbował się dodstać z przeglądarki pod adresem https://x.domain.com:2999. 
Można też użyć socks proxy dostępnego w Firefox, dodając proxy localhost na porcie 2999. 

ssh -L 2999:localhost:2999 root@jumpbox.local -t ssh -L 2999:localhost:8080 -p 2222 root@app01.local

To jest przykład zestawienia tunelu, pisałem z głowy, nie wiem czy po odpaleniu zadziała od razu. Być może wymaga jakichś poprawek, nie miałem gdzie tego przetestować, ale generalnie szedł bym w tym kierunku. 